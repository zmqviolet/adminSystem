import axios from 'axios'
import 'babel-polyfill'
import Api from '@/config.js'
import {
  Message
} from 'iview'
import store from '@/store'
import qs from 'qs'
import cookie from '../until/cookie.js'
const baseUrl = Api.baseUrl
axios.defaults.withCredentials = true
let webReponseTime = cookie.get('webReponseTime')
let CancelToken = axios.CancelToken
let loading = true
// 拦截request,设置全局请求为ajax请求
axios.interceptors.request.use(function (config) {
  if (loading) {
    // store.commit('UPDATE_LOADING', true)
  }
  //   config.headers['prelogid'] = window.xesWeb_eventLog.getCookie_log('prelogid') || ''
  config.headers['X-Requested-With'] = 'XMLHttpRequest'
  config.timeout = webReponseTime * 1000 || 30000 // 超时时间
  config.url = baseUrl + config.url
  let cancelGroupName
  if (config.method === 'post') {
    if (config.data && config.data.cancelGroupName) { // post请求ajax取消函数配置
      cancelGroupName = config.data.cancelGroupName
    }
    config.data = qs.stringify(config.data)
  } else {
    if (config.params && config.params.cancelGroupName) { // get请求ajax取消函数配置
      cancelGroupName = config.params.cancelGroupName
    }
  }
  if (cancelGroupName) {
    if (axios[cancelGroupName] && axios[cancelGroupName].cancel) {
      axios[cancelGroupName].cancel()
    }
    config.cancelToken = new CancelToken(c => {
      axios[cancelGroupName] = {}
      axios[cancelGroupName].cancel = c
    })
  }
  return config
})

// 拦截响应response，并做一些错误处理
axios.interceptors.response.use(function (response) {
  if (loading) {
    // store.commit('UPDATE_LOADING', false)
  }
  const data = response.data
  // 根据返回的code值来做不同的处理（和后端约定）
  switch (data.status) {
    case 9: // 登录失效 刷新页面
      if (navigator.userAgent.toLocaleLowerCase().indexOf('xescef') > -1) {
        //   window.onInvokeQtFunction('loginFailed')
      } else {
        Message.error(response.data.message)
        // window.location.reload()
      }
      break
  }
  return data
},

function (err) { // 这里是返回状态码不为200时候的错误处理
  // flag = true
  // clearTimeout(timer)
  // loading && loading.close()
//   store.commit('UPDATE_LOADING', false)
  if (err.code === 'ECONNABORTED' && err.message.indexOf('timeout') !== -1) { // 超时处理
    //   window.x5home_dialogVisible = true
  }
  // if (err && err.response) {
  //   switch (err.response.status) {
  //     case 400:
  //       err.message = '请求错误'
  //       break
  //     case 401:
  //       err.message = '未授权，请登录'
  //       break
  //     case 403:
  //       err.message = '拒绝访问'
  //       break
  //     case 404:
  //       err.message = '请求地址出错: ' + err.response.config.url
  //       break
  //     case 408:
  //       err.message = '请求超时'
  //       break
  //     case 500:
  //       err.message = '服务器内部错误'
  //       break
  //     case 501:
  //       err.message = '服务未实现'
  //       break
  //     case 502:
  //       err.message = '网关错误'
  //       break
  //     case 503:
  //       err.message = '服务不可用'
  //       break
  //     case 504:
  //       err.message = '网关超时'
  //       break
  //     case 505:
  //       err.message = 'HTTP版本不受支持'
  //       break
  //     default:
  //   }
  // }
  return Promise.reject(err)
})
export default axios
