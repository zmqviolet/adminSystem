import Vue from 'vue'
import Router from 'vue-router'
// import MenuM from '@/views/adminUser/menuM'
// import RoleM from '@/views/adminUser/roleM'
import routes from './routers'
// import OperLog from '@/views/adminUser/operLog'
// import Index from '@/views/index'
// import NotFound from '@/common/notFound/notFound'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes
  // base:'/base', // 基础路由 ，/base/index
})
export default router
