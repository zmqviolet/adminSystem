import Main from '@/components/main'
// import parentView from '@/components/parent-view'
export default [{
    path: '/login',
    name: 'Login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/views/login/login')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [{
      path: '/home',
      name: 'home',
      meta: {
        hideInMenu: true,
        title: '首页',
        notCache: true,
        icon: 'md-home'
      },
      component: () => import('@/views/index')
    }]
  },
  {
    path: '/teacher',
    name: 'teacher',
    meta: {
      icon: 'md-funnel',
      title: '教师管理',
    //   hideInMenu: true

    },
    component: Main,
    children: [{
        path: 'face-teacher',
        name: 'face-teacher',
        meta: {
          icon: 'md-funnel',
          title: '面授教师'
        },
        component: () => import('@/views/teacher/face-teacher/face-teacher.vue')
      },
      {
        path: 'hall-teacher',
        name: 'hall-teacher',
        meta: {
          icon: 'md-funnel',
          title: '大讲堂教师'
        },
        component: () => import('@/views/teacher/hall-teacher/hall-teacher.vue')
      },
      {
        path: 'online-teacher',
        name: 'online-teacher',
        meta: {
          icon: 'md-funnel',
          title: '线上教师'
        },
        component: () => import('@/views/teacher/online-teacher/online-teacher.vue')
      }
    ]
  },
  {
    path: '/task',
    name: 'task',
    meta: {
    //   access: ['super_admin'],  // 管理员才能看的页面
      icon: 'md-funnel',
      title: '作业管理',
    //   hideInMenu: true
    },
    component: Main,
    children: [{
        path: 'task-manage',
        name: 'task-manage',
        meta: {
          icon: 'md-funnel',
          title: '作业管理'
        },
        component: () => import('@/views/task/task-manage/task-manage.vue')
      },
      {
        path: 'task-list',
        name: 'task-list',
        meta: {
          icon: 'md-funnel',
          title: '作业批改列表'
        },
        component: () => import('@/views/task/task-list/task-list.vue')
      },
      {
        path: 'task-data',
        name: 'task-data',
        meta: {
          icon: 'md-funnel',
          title: '作业数据'
        },
        component: () => import('@/views/task/task-data/task-data.vue')
      }
    ]
  }
]
