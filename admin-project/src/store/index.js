import Vue from 'vue'
import Vuex from 'vuex'
import defaultState from './state/state'
import mutations from './mutations/mutations'
import getters from './getters/getters'
import actions from './actions/actions'
import user from './module/user'
Vue.use(Vuex)
const isDev = process.env.NODE_ENV === 'dev'
export default new Vuex.Store({
  strict: isDev,
  state: defaultState,
  // 修改数据
  mutations,
  getters,
  actions,
  modules:{
    user
  }
})
