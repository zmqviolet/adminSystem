import {
  getBreadCrumbList,
  getHomeRoute
} from '@/assets/until/getFun'
import config from '@/components/main/config'
const {
  homeName
} = config
export default {
    // 获取面包屑集合
  setBreadCrumb(state, route) {
    state.breadCrumbList = getBreadCrumbList(route, state.homeRoute)
  },
  // 获取路由集合
  setHomeRoute(state, routes) {
    state.homeRoute = getHomeRoute(routes, homeName)
  }
}
